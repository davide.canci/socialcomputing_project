//DICHIARAZIONE VARIABILI
var steps = []; //Array che contiene lo storico degli step
var step = 0; //Step corrente
const N_STEP_MAX = 9;
const NO_DOCS = 7 //numero img da classificare
var failure = 0;
var times = [];

const SOGLIA_TIME = 2; //soglia minima per svolgere un task in modo decente
var doc_start_time = Date();

//Path delle immagini da classificare
const IMG_PATH = ["",
                    "https://kevinr.s3.us-east-2.amazonaws.com/landscapes/2.jpg",
                    "https://kevinr.s3.us-east-2.amazonaws.com/landscapes/7.jpg",
                    "https://kevinr.s3.us-east-2.amazonaws.com/landscapes/5.jpg",
                    "https://kevinr.s3.us-east-2.amazonaws.com/landscapes/6.jpg",
                    "https://kevinr.s3.us-east-2.amazonaws.com/landscapes/4.jpg",
                    "https://kevinr.s3.us-east-2.amazonaws.com/landscapes/3.jpg",
                    "https://kevinr.s3.us-east-2.amazonaws.com/landscapes/1.jpg"];

//testo dello risposte (costante)
RADIO_ANSWER1 = "Mare"
RADIO_ANSWER2 = "Pianura"
RADIO_ANSWER3 = "Montagna"
RADIO_ANSWER4 = "Città"

//var per il salvataggio delle risposte già date
var previus_answers = [];
var previus_answers_labeled = [];
//var questionario - setto a -1 come valore di default
var risposta_gender = -1;
var risposta_age = -1;
var risposta_school = -1;
 
//GOLD QUESTIONS
var gold1
const CORRECT_GOLD1 = 4;

var gold2
const CORRECT_GOLD2 = 1;



// Check su tempo impiegato
function check_time(){
    for (i=1; i<=NO_DOCS; i++){
        if (times[i] < SOGLIA_TIME){
            console.log("un task e stato fatto troppo in fretta")
            return false;
        }else{
            return true;
        }
    }
    
}

//Funzione che uso per controllare se l'utente ha inserite e compilto il modo corretto tutti i campi
function check_input(step){
    switch (step) {
        case 0:
            doc_end_time = (new Date().getTime() - doc_start_time.getTime()) / 1000; //calcolo il tempo che ci ha messo a fare il task
            times[step] = doc_end_time; //lo metto nell'array dei tempi (che controllero alla fine)

            if(risposta_gender != -1 && risposta_age != -1 && risposta_school != -1){
                return true;
            }else{
                alert("Compila tutti i campi!");
                return false;
            }
            break;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
            doc_end_time = (new Date().getTime() - doc_start_time.getTime()) / 1000; //calcolo il tempo che ci ha messo a fare il task
            times[step] = doc_end_time; //lo metto nell'array dei tempi (che controllero alla fine)

            if(previus_answers[step] != null){
                return true;
            }else{
                alert("Scegli un radio button!");
                return false;
            }

    }
}


//FUNZIONI COLLEGATE AI PULSANTI DI NAVIGAZIONE SOLO PER AGGIORNAMENTO TASK
$("#btnNext").click(function(evt) {
    if(check_input(step)){
        if(step>=0){
            step++;
            updateStep();
        }
    }

});

$("#btnBack").click(function(evt) {
    if(step>0){
        step--;
        updateStep();
    }else{ //non posso andare in negativo
        step = 0;
        updateStep();
    }
});

$("#btnRetry").click(function(evt) { //riparti dal primo step
    step = 1;
    updateStep();
});


 ///ALTRE CHE SERVONO ..

 $("#btnBack").click(function(){
        if (previus_answers[step]!== null){ //se non è vuoto
            $("input[name=imgAnswer][value='" + previus_answers[step] + "']").prop('checked', true); //checko il radio
        }
});

$("#btnNext").click(function(){
    if (previus_answers[step]!== null){ //se non è vuoto
        $("input[name=imgAnswer][value='" + previus_answers[step] + "']").prop('checked', true); //checko il radio
    }
});
//

// Nascondo tutti i passi, all'inizio
function hideAll() {
    $("#quest").hide();
    $("#task_page").hide();
    $("#errorBox").hide();
    $("#retryBox").hide();
    $("#endBox").hide();
    $("#btnBox").hide();
    $("#btnBack").hide();
    $("#btnNext").hide();
}

function checkFinale(){ //check delle gold question e del tempo!
    
    var gold_score = 0;
    if(gold1 == CORRECT_GOLD1){
        gold_score++;
    }
    if(gold2 == CORRECT_GOLD2){
        gold_score ++;
    }
    if(gold_score >=1){ //se le gold question sono ok
        if(check_time()){ //controllo il tempo che e stato a fare i task
            return true;
        } else{ //se è stato troppo veloce torno false
            return false;
        }
        
    }
    
}


function updateStep(){
    console.log("Sei allo step: " + step);
    switch(step){
        case 0: //Caso 0 carico il questionario
            steps.push(step); //Aggiorno lo storico degli step
            doc_start_time = new Date(); //Segno la data di inizio task
            hideAll(); //Nascondo tutto in modo da caricare solo quello che mi serve
            //Carico gli elementi che compongono il questionario
            $("#quest").show(); //Questionario
            //Pulsanteria 
            $("#btnBox").show();
            $("#btnNext").show();

            //Event listeners, per lo store delle info del questionario
            $('input[type=radio][name=age]').on('change', function() {
                $("#btnNext").show();
                risposta_age = $("input[name=age]:checked").val(); 
                console.log("Current answer age:" + risposta_age);
                });
            /* Gender */
            $('input[type=radio][name=gender]').on('change', function() {
                $("#btnNext").show();
                risposta_gender = $('input[name=gender]:checked').val(); 
                console.log("Current answer gender:" + risposta_gender);
                });
            /* School */
            $('input[type=radio][name=school]').on('change', function() {
                $("#btnNext").show();
                risposta_school = $('input[name=school]:checked').val(); 
                console.log("Current answer school:" + risposta_school);
                });
               
            break; 
            case 1: //img1
            case 2: //img2
            case 3: //img3
            case 4: //img4
            case 5: //img5
            case 6:
            case 7:
                steps.push(step); //Aggiorno lo storico degli step
                doc_start_time = new Date(); //Segno la data di inizio task
                hideAll();
                $("input[type=radio][name=imgAnswer]").prop('checked', false); //Resetto lo stato dei radiobutton
                //OK! RIEMPIO DINAMICAMENTE IL CONTENUTO DELL'IMMAGINE e dei RADIO!
                //$("#taskImg").attr("src", IMG_PATH[step]);

                $("#taskImg").attr("src", $("#img"+step).val());
                
                
                if(step == 6){
                    $("#taskImg").attr("src", $("#gold1"));
                }else if(step == 7){
                    $("#taskImg").attr("src", $("#gold2"));
                }
                
                $("#radio_answer1").html( RADIO_ANSWER1 );
                $("#radio_answer2").html( RADIO_ANSWER2 );
                $("#radio_answer3").html( RADIO_ANSWER3 );
                $("#radio_answer4").html( RADIO_ANSWER4 );


                //Mostro quello che mi serve
                $('#page_title').html("Statement " + step + " of " + NO_DOCS); //Aggiorno il titolo
                $("#task_page").show();
                $("#btnBox").show();
                $("#btnNext").show();
                $("#btnBack").show();

                if (step == N_STEP_MAX){ //se sono all'ultimo step, cambio la label del pulsante in:
                    $("#btnNext").html('Finish | Check Results');
                }else{
                    $("#btnNext").html('Next');
                }

                $('input[type=radio][name=imgAnswer]').on('change', function() { //Salvo in realtime la risposta dell'utente
                    previus_answers[step] = $("input[name=imgAnswer]:checked").val();
                    if(step == 6){
                        gold1 = $("input[name=imgAnswer]:checked").val();
                    } else if(step == 7){
                        gold2 = $("input[name=imgAnswer]:checked").val();
                    }
                    console.log("Step:" + step + " Label:" + previus_answers[step]);
                });
            break;
            // case 6: //goldQuestion1
            // case 7: //goldQuestion2
                
            case 8: //Controllo finale perchè dopo questo step invio i dati ad AMAZON, nel caso di err- o gli faccio fare di nuovo il task o lo banno
                steps.push(step)
                if(checkFinale()){
                    step++;
                    updateStep()
                }else{
                    failure++; //aggiorno i fallimenti
                    if(failure >= 3){
                        console.log("BANNATO! TENTATIVI ESAURITI");
                        hideAll();
                        $("#errorBox").show();
                    }else{
                        console.log('Controllo finale warning');
                        hideAll();
                        $("#retryBox").show();
                      
                    }
                }
                break;
            case 9:
                steps.push(step);
                hideAll()
                //Mostro l'ending
                $("#endBox").show();
                break;
    }

}
 //----------START!!!!------------
$( document ).ready(function() {
    console.log("-----START------");
    updateStep();
});